require 'test_helper'

class UsersSingupTest < ActionDispatch::IntegrationTest
  test "invalid singup information" do
    get singup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name: "", email: "user@invalid", password: "foo", password_confirmation: "bar" } }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.alert.alert-danger'
  end
  test "valid singup information" do
    get singup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name: "example user", email: "user@example.com", password: "P@ssw0rd", password_confirmation: "P@ssw0rd" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
    assert_not flash.empty?
  end
end
